import qualified Data.ByteString.Lazy as BL
import Data.Binary.Put
import Data.Word
import Data.Char
import Debug.Trace
import Data.Either
import Data.List.Split
import Data.Map (Map)
import qualified Data.Map as Map

type Chunk = Word8
type Error = String

data SymbolTable = Table (Map String Integer)

putInstruction :: Chunk -> Put
putInstruction ins = do
  putWord8 ins

main = do
  assembly <- getContents
  let result = assemble assembly
  case result of
    Left ls -> putStrLn (unlines ls)
    Right rs -> mapM_ (BL.putStr . runPut . putInstruction) rs

assemble :: String -> Either [Error] [Chunk]
assemble input = getResult (map assembleSingle (realLines input))
  where symtab = symbolTable input
        assembleSingle = assembleInstruction (Table symtab)

assemblyLines :: String -> [(String, Integer)]
assemblyLines s = zip (realLines s) [0..]

realLines :: String -> [String]
realLines s = filter (not . isLabel) (filter isRealLine (map trim (lines s)))

trim :: String -> String
trim = takeWhile (not . isSpace) . dropWhile isSpace

symbolTable :: String -> Map String Integer
symbolTable assembly = Map.fromList $ labels (lines assembly)

labels :: [String] -> [(String, Integer)]
labels xs = tail $ scanl (\(_, count) (name, jump) -> (removeColon name, count + jump)) ("", 0) (countsBetween isLabel realLines)
  where realLines = filter isRealLine (map trim xs)

countsBetween :: (a -> Bool) -> [a] -> [(a, Integer)]
countsBetween pred xs = zip (filter pred xs) (map (fromIntegral . length) (splitWhen pred xs))

labelValue :: String -> String
labelValue s = if isLabel s then s else ""

isLabel :: String -> Bool
isLabel s = last s == ':'

isRealLine :: String -> Bool
isRealLine line = any (not . isSpace) withoutComments
  where withoutComments = takeWhile (not . (==';')) line

getResult :: [Either a b] -> Either [a] [b]
getResult xs = case partitionEithers xs of
  ([], rs) -> Right rs
  (ls,  _) -> Left ls

assembleInstruction :: SymbolTable -> String -> Either Error Chunk

assembleInstruction _ "add" = Right 1
assembleInstruction _ "sub" = Right 2
assembleInstruction _ "mul" = Right 3
assembleInstruction _ "div" = Right 4
assembleInstruction _ "mod" = Right 5
assembleInstruction _ "dup" = Right 6
assembleInstruction _ "brn" = Right 7
assembleInstruction _ "lit" = Right 8
assembleInstruction _ "out" = Right 9
assembleInstruction _ "jmp" = Right 10
assembleInstruction _ "ret" = Right 11
assembleInstruction _ "drp" = Right 12
assembleInstruction _ "swp" = Right 13
assembleInstruction _ "rol" = Right 14
assembleInstruction _ "slt" = Right 15

assembleInstruction (Table tab) (':':labelName) = case Map.lookup labelName tab of
  Just labelLine -> Right (fromInteger labelLine)
  Nothing        -> Left ("Unrecognized label " ++ labelName ++ ".")

assembleInstruction _ s = case readChunk s of
  Just result -> Right result
  Nothing     -> Left ("Could not assemble '" ++ s ++ "'")

readChunk :: String -> Maybe Chunk
readChunk s = case reads s of
  [(x, "")] -> Just (toTwosComplement x)
  _         -> Nothing

toTwosComplement :: Chunk -> Chunk
toTwosComplement x
  | x > 128 = 384 - x
  | otherwise = x

removeColon :: String -> String
removeColon = init
