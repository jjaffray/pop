I have no idea what I am doing, so everything here is probably extremely bad.

Pop is a stack machine/instruction set.

```
The instructions so far include:
ADD: Add the two values on top of the stack
SUB: Subtract the two values on top of the stack
MUL: Multiply the two values on top of the stack
DIV: Divide the two values on top of the stack
MOD: Divide the two values on top of the stack
DUP: Copy the value on top of the stack
JMP: If the top value is not zero, jump by the second-to-top value
LIT: Push the next value onto the stack as a LITeral
OUT: Output the top byte
```

There is a lot of stuff not there yet, like memory access.

Labels are created by
```
label_name:
```
and referenced by
```
:label_name
```
Comments begin with a semicolon.


The eventual goal is to write a compiler (probably for Forth) to this.
Right now included is a vm for it (vm.c) and a very basic assembler (assembler.hs)

Why?
----

Because I should be studying
