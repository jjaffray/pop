#include <stdio.h>
#include <stdlib.h>

#define MEM_SIZE 64
#define STACK_SIZE 64
#define RETURN_STACK_SIZE 64
#define DEBUG 0

typedef char CHUNK;

struct Stack
{
  int top;
  CHUNK array[STACK_SIZE];
};

enum instructions {
  ADD = 1,
  SUB = 2,
  MUL = 3,
  DIV = 4,
  MOD = 5,
  DUP = 6,
  BRN = 7,
  LIT = 8,
  OUT = 9,
  JMP = 10,
  RET = 11,
  DRP = 12,
  SWP = 13,
  ROL = 14,
  SLT = 15,
};

void push(struct Stack *stack, CHUNK val);
void run(CHUNK *mem);
void execute(CHUNK instruction, struct Stack *stack, struct Stack *return_stack, CHUNK *mem, int *pc);
void print_stack(struct Stack *s);

int main(int argc, char** argv) {
  FILE *fp;
  int c;
  CHUNK *mem = malloc(MEM_SIZE * sizeof(CHUNK));

  if (!(fp = fopen(argv[1], "rt"))) {
    perror(argv[1]);
    return 1;
  }
  int i = 0;
  while ((c = fgetc(fp)) != EOF) {
    mem[i] = c;
    i++;
  }
  fclose(fp);
  run(mem);
  free(mem);
  return 0;
}

void init_stack(struct Stack *s) {
  s->top = 0;
  for (int i = 0; i < STACK_SIZE; i++) {
    s->array[i] = 0;
  }
}

void run(CHUNK *mem) {
  struct Stack stack;
  init_stack(&stack);

  struct Stack return_stack;
  init_stack(&return_stack);
  push(&return_stack, 255);

  int i = 0;
  while(i<MEM_SIZE) {
    CHUNK next_instruction = mem[i];
    i++;
    execute(next_instruction, &stack, &return_stack, mem, &i);
  }
}

void print_mem(CHUNK *mem) {
  for (int i = 0; i < STACK_SIZE; i++) {
    printf("%d ", mem[i]);
  }
  printf("\n");
}

void print_stack(struct Stack *s) {
  for (int i = 0; i < STACK_SIZE; i++) {
    printf("%d ", s->array[i]);
  }
  printf("\n");
}

void push(struct Stack *stack, CHUNK val) {
    stack->array[stack->top] = val;
    stack->top = stack->top + 1;
}

CHUNK pop(struct Stack *stack) {
  if (stack->top == 0) {
    printf("POPPING EMPTY STACK!\n");
    exit(1);
  }
  stack->top = stack->top - 1;
  CHUNK val = stack->array[stack->top];
  stack->array[stack->top] = 0;
  return val;
}

CHUNK peek(struct Stack *stack) {
  CHUNK val = stack->array[stack->top - 1];
  return val;
}

void execute(CHUNK instruction, struct Stack *stack, struct Stack *return_stack, CHUNK *mem, int *pc) {
  CHUNK val1;
  CHUNK val2;
  CHUNK val3;
  if (DEBUG) {
    printf("===============\n");
    printf("pc: %d\n", *pc);
    printf("instruction: %d\n", instruction);
    printf("stack: \n");
    print_stack(stack);
    printf("return stack: \n");
    print_stack(return_stack);
    printf("mem: \n");
    print_mem(mem);
  }
  switch (instruction) {
    case LIT:
      push(stack, mem[*pc]);
      *pc = *pc + 1;
      break;
    case ADD: 
      val1 = pop(stack);
      val2 = pop(stack);
      push(stack, val1+val2);
    break;
    case MUL: 
      val1 = pop(stack);
      val2 = pop(stack);
      push(stack, val1*val2);
    break;
    case DIV: 
      val1 = pop(stack);
      val2 = pop(stack);
      push(stack, val2/val1);
    break;
    case MOD: 
      val1 = pop(stack);
      val2 = pop(stack);
      push(stack, val2%val1);
    break;
    case SUB: 
      val1 = pop(stack);
      val2 = pop(stack);
      push(stack, val2-val1);
    break;
    case DUP:
      val1 = peek(stack);
      push(stack, val1);
    break;
    case BRN:
      val1 = pop(stack);
      val2 = pop(stack);
      if (val2 != 0) {
        *pc = val1;
      }
    break;
    case OUT:
      val1 = pop(stack);
      printf("%c", val1);
    break;
    case RET:
      val1 = pop(return_stack);
      if (val1 == -1) {
        exit(0);
      } else {
        *pc = val1;
      }
    break;
    case JMP:
      val1 = pop(stack);
      push(return_stack, *pc);
      *pc = val1;
    break;
    case DRP:
      pop(stack);
    break;
    case SWP:
      val1 = pop(stack);
      val2 = pop(stack);
      push(stack, val1);
      push(stack, val2);
    break;
    case ROL:
      val1 = pop(stack);
      val2 = pop(stack);
      val3 = pop(stack);
      push(stack, val1);
      push(stack, val3);
      push(stack, val2);
    break;
    case SLT:
      val1 = pop(stack);
      val2 = pop(stack);
      if (val2 < val1) {
        push(stack, 1);
      } else {
        push(stack, 0);
      }
    break;
    default:
      printf("Unrecognized instruction %d\n", instruction);
      exit(1);
    break;
  }
}
